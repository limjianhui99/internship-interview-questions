
# CG Crypto Dashboard

## Instructions

### Online access
This project is hosted via Firebase on [https://cg-crypto-dashboard.web.app](https://cg-crypto-dashboard.web.app)

### Local access
1. Clone this repository
2. Open index.html

## Description
This project is a simple crypto dashboard that performs the following functions
1. Displays the top 100 ranked coins/tokens
2. Displays their symbol, price, volume and sparkline data (7 days)
3. Displays options to switch display currency between USD/MYR/BTC
4. Allows users to "favourite" a coin/token and display only those tokens

## Disclaimer

[CoinGecko API](https://www.coingecko.com/en/api) - Live crypto data

[Google Line Chart](https://developers.google.com/chart/interactive/docs/gallery/linechart) - Line graphs

All image assets used in this project are free to use (refer to "assets_disclaimer.txt")