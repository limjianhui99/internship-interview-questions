"use strict";

// Favourite coins functions

const FAVOURITE_COINS = [];
const FAVOURITE_BUTTON_PREFIX = `${FAVOURITE_ID_PREFIX}button_`;

/**
 * Visually updates favourite buttons of favourite coins by id
 */
function updateButtonsWithFavourites() {
    const favouriteCoinsIds = getFromLocalStorage(FAVOURITES_KEY)
    if (favouriteCoinsIds != null && typeof(favouriteCoinsIds) === "object") {
        favouriteCoinsIds.forEach(function(coinId) {
            let favouriteButtonId = `${FAVOURITE_BUTTON_PREFIX}${coinId}`;
            let button = document.getElementById(favouriteButtonId);
            if (button !== null && button.value === "false") {
                // Favourite the coin
                button.value = true;

                // The button click now removes from favourites
                button.className = "unfavourite_button";
            }
        })
    }

}

/**
 * Used as on click function. 
 * If button is not a favourite, favourite
 * If button is already a favourite, unfavourite
 * @param {object} button reference to a favourite button
 */
function toggleFavourite(button, coinId) {
    const isFavouriteIconLink = 'url("assets/star_filled.svg")';
    const notFavouriteIconLink = 'url("assets/star_border.svg")'
    let isAFavourite = button.value === "true";
    if (isAFavourite) {
        // Unfavourite the coin
        removeCoinIdFromFavourites(coinId);
        console.log("Removed coin from favourites");
        
        // The button click now adds to favourites
        button.className = "favourite_button";
    } else {
        // Favourite the coin
        addCoinIdToFavourites(coinId);
        console.log("Added a coin to favourites");

        // The button click now removes from favourites
        button.className = "unfavourite_button";
    }
    // Toggle button's truth value
    button.value = !isAFavourite;
}
