"use strict";

const FAVOURITES_KEY = "favourites";
const DISPLAY_CURRENCY_KEY = "currency";

const DEFAULT_DISPLAY_CURRENCY = "usd";
const DISPLAY_CURRENCY_LIST = ["usd", "myr", "btc"];

/**
 * Converts data into JSON and stores it 
 * @param {string} key should use a const key from this file 
 * @param {*} dataToSave any type of data
 */
function saveToLocalStorage(key, dataToSave) {
    if (dataToSave !== null) {
        dataToSave = JSON.stringify(dataToSave);
        localStorage.setItem(key, dataToSave);
    }
}

/**
 * Either returns existing parsed data or null
 * @param {string} key should use a const key from this file 
 */
function getFromLocalStorage(key) {
    let retrievedData = localStorage.getItem(key);
    if (retrievedData !== null) {
        retrievedData = JSON.parse(retrievedData);
    }
    return retrievedData;
}

/**
 * Adds coin id to favourites array (creates a new array if it does not exist)
 * @param {string} coinId id of a coin eg. "bitcoin" 
 */
function addCoinIdToFavourites(coinId) {
    let dataToSave;
    let coinAlreadyFavourite = false;
    let existingData = getFromLocalStorage(FAVOURITES_KEY);
    if (existingData === null) {
        // Initialize a new array
        dataToSave = [];
    } else {
        // Check if coin already exists in existing data
        dataToSave = existingData
        coinAlreadyFavourite = dataToSave.includes(coinId);
        
    }

    if (coinAlreadyFavourite) {
        // Should be an unlikely console log unless toggle has issues
        console.log(`${coinId} is already favourited`);
    } else {
        // Add new favourite coin id and save the updated data
        dataToSave.push(coinId);
        saveToLocalStorage(FAVOURITES_KEY, dataToSave);
    }
}

/**
 * Removes a coin id from favourites array if possible
 * @param {*} coinId id of a coin eg. "bitcoin"
 */
function removeCoinIdFromFavourites(coinId) {
    let existingData = getFromLocalStorage(FAVOURITES_KEY);
    if (existingData !== null && typeof(existingData) === "object") {
        if (existingData.includes(coinId)) {
            // Remove favourite coin id and save the updated data
            let indexOfCoin = existingData.indexOf(coinId)
            existingData.splice(indexOfCoin, 1); // do not take return value
            saveToLocalStorage(FAVOURITES_KEY, existingData);
        } else {
            // Should be an unlikely console log unless toggle has issues
            console.log(`${coinId} does not exist in favourites`);
        }
    }
}

/**
 * Returns list of favourite coin ids or null
 */
function retrieveFavouriteCoinIds() {
    return getFromLocalStorage(FAVOURITES_KEY);
}

/**
 * Sets display currency
 * @param {string} displayCurrency 
 */
function saveDisplayCurrency(displayCurrency) {
    saveToLocalStorage(DISPLAY_CURRENCY_KEY,displayCurrency);
}

/**
 * Gets display currency or returns default
 */
function retrieveDisplayCurrency() {
    const existingDisplayCurrency = getFromLocalStorage(DISPLAY_CURRENCY_KEY);
    let returnDisplayCurrency = DEFAULT_DISPLAY_CURRENCY;
    if (existingDisplayCurrency !== null && typeof(existingDisplayCurrency) === "string") {
        if (DISPLAY_CURRENCY_LIST.includes(existingDisplayCurrency)) {
            returnDisplayCurrency = existingDisplayCurrency;
        }
    } 
    return returnDisplayCurrency;
}