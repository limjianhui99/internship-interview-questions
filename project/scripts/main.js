"use strict";

// Main file to interact with other JS files

const HOME_PAGE = "home.html";
const FAVOURITES_PAGE = "favourites.html";

/**
 * Initialize page with data
 */
async function initializePage() {
    // Display loader
    setTableLoaderDisplayBlock(true);
    // Obtain market data and populate coin list
    try {
        const apiCallFunction = getAPICallFunctionBasedOnPage();
        if (apiCallFunction !== null) {
            const displayCurrency = retrieveDisplayCurrency();
            const marketData = await apiCallFunction(displayCurrency);
            if (marketData !== null) {
                let table = document.getElementById("main_table");
                populateCoinList(marketData);
                populateMainTable(table, displayCurrency);
                // Delay, then reveal table and remove loader
                setTimeout(function() {
                    setDisplayCurrencyMenuText();
                    setTableLoaderDisplayBlock(false);
                    setTableContainerVisibility(true);
                }, 1500);
            } else {
                // Stop the loader
                setTableLoaderDisplayBlock(false);
            }
        } else {
            // Developer debug: Error should have been thrown
            const errorMessage = "Developer: Error with API function call to servers";
            showTableErrorMessage(true, errorMessage);
            throw new Error(errorMessage);
        }
    } catch(errorMessage) {
        // Log error message, add a return to home button and stop the loader
        console.log(errorMessage);
        const currentPage = location.href.split("/").slice(-1)[0];
        if (currentPage !== HOME_PAGE) {
            // Only add return home button if not currently on home
            addReturnHomeButton();
        }
        setTableLoaderDisplayBlock(false);
    }
}

function getAPICallFunctionBasedOnPage() {
    const currentPage = location.href.split("/").slice(-1)[0];
    let apiCallFunction;
    if (currentPage === HOME_PAGE) {
        apiCallFunction = getMarketData;
    } else if (currentPage === FAVOURITES_PAGE) {
        apiCallFunction = getFavouritesMarketData;
    }

    return apiCallFunction;
}

/**
 * Adds a return home button to general button container if it does not exist
 */
function addReturnHomeButton() {
    // Check if a return home button already exists
    const returnHomeButtonId = "return_home_button";
    const generalButtonContainerId = "general_button_container";
    let returnHomeButton = document.getElementById(returnHomeButtonId);
    if (returnHomeButton === null) {
        // Build return home button and append as general button container's child
        const generalButtonContainer = document.getElementById(generalButtonContainerId);
        const returnHomeFunction = function() {location.href='home.html'};
        
        returnHomeButton = document.createElement("button");
        returnHomeButton.className = "general_button";
        returnHomeButton.id = returnHomeButtonId;
        returnHomeButton.onclick = returnHomeFunction;
        returnHomeButton.innerText = "Return home";
        generalButtonContainer.appendChild(returnHomeButton); 
    }
}
