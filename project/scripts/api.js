"use strict";

// API GET and POST request functions
// Documentation: https://www.coingecko.com/en/api#explore-api

const API_URL = "https://api.coingecko.com/api/v3";
const OK_STATUS = 200;
const COIN_LIST = [];


/**
 * Pings CoinGecko servers and logs status code
 */
async function ping()  {
    const endpoint = "/ping";
    const queryString = `${API_URL}${endpoint}`;
    const response = await fetch(queryString);
    console.log(`CoinGecko status: ${response.status}`);
}

/**
 * GET coin market data in vs_currency
 * Returns null if there is an API error
 * @param {string} vs_currency currency symbols such as usd", "myr", or "btc"
 * @param {string} priceChangePercentage timespan of price percentage change, refer below for valid values
 * @param {string} order order of market data, refer below for valid values
 * @param {int} page page number
 * @param {boolean} sparkline flag to retrieve 7 day sparkline
 * @return {*} marketData object or null
 * 
 * Valid values of priceChangePercentage
 * >> 1h, 24h, 7d, 14d, 30d, 200d, 1y
 * Example: "1h, 24h, 7d"
 * 
 * Valid values of order
 * >> market_cap_desc, gecko_desc, gecko_asc, market_cap_asc, market_cap_desc, volume_asc, volume_desc, id_asc, id_desc
 */
async function getMarketData(vsCurrency, priceChangePercentage="24h", order="market_cap_desc", sparkline="true", page=1) {
    const resultsPerPage = 100;
    let marketData = null;

    // Build queryString and make a GET request
    const endpoint = "/coins/markets";
    const currencyParamater = `&vs_currency=${vsCurrency}`;
    const priceChangePercentageParameter = `&price_change_percentage=${priceChangePercentage}`;
    const orderParameter = `&order=${order}`
    const pageParameter = `page=${page.toString()}`;
    const perPageParameter = `&per_page=${resultsPerPage.toString()}`
    const sparklineParameter = `&sparkline=${sparkline.toString()}`;
    const queryString = `${API_URL}${endpoint}?${currencyParamater}${priceChangePercentageParameter}
                            ${orderParameter}${pageParameter}${perPageParameter}${sparklineParameter}`
    try {
        const response = await fetch(queryString);
        if (response.status === OK_STATUS) {
            marketData = await response.json();
        }
    } catch (error) {
        console.log(error);
    }
    return marketData;

}

/**
 * GET coin market data in vs_currency
 * Returns null if there is an API error
 * @param {string} vs_currency currency symbols such as usd", "myr", or "btc"
 * @param {object} ids specific coin ids
 * @param {string} priceChangePercentage timespan of price percentage change, refer below for valid values
 * @param {string} order order of market data, refer below for valid values
 * @param {int} page page number
 * @param {boolean} sparkline flag to retrieve 7 day sparkline
 * @return {*} marketData object or null
 * 
 * Valid values of priceChangePercentage
 * >> 1h, 24h, 7d, 14d, 30d, 200d, 1y
 * Example: "1h, 24h, 7d"
 * 
 * Valid values of order
 * >> market_cap_desc, gecko_desc, gecko_asc, market_cap_asc, market_cap_desc, volume_asc, volume_desc, id_asc, id_desc
 */
async function getMarketData(vsCurrency, ids=null, priceChangePercentage="24h", order="market_cap_desc", sparkline="true", page=1) {
    const resultsPerPage = 100;
    let marketData = null;

    // Build queryString and make a GET request
    const endpoint = "/coins/markets";
    const currencyParamater = `&vs_currency=${vsCurrency}`;
    const idParameter = ids !== null ? `&ids=${ids}` : "";
    const priceChangePercentageParameter = `&price_change_percentage=${priceChangePercentage}`;
    const orderParameter = `&order=${order}`
    const pageParameter = `page=${page.toString()}`;
    const perPageParameter = `&per_page=${resultsPerPage.toString()}`
    const sparklineParameter = `&sparkline=${sparkline.toString()}`;
    const queryString = `${API_URL}${endpoint}?${currencyParamater}${idParameter}${priceChangePercentageParameter}
                            ${orderParameter}${pageParameter}${perPageParameter}${sparklineParameter}`
    try {
        const response = await fetch(queryString);
        if (response.status === OK_STATUS) {
            marketData = await response.json();
        }
    } catch (error) {
        // Display error message and throw an error
        const errorMessage = "Ooops... Unable to obtain data";
        showTableErrorMessage(true, errorMessage);
        throw new Error(errorMessage);
    }
    return marketData;

}

/**
 * Calls getMarketData with list of favourite coins
 * Throws an error if there are no favourite coins
 */
async function getFavouritesMarketData() {
    const displayCurrency = retrieveDisplayCurrency();
    let favouriteCoinIds = retrieveFavouriteCoinIds();
    let marketData = null;
    if (favouriteCoinIds === null) {
        favouriteCoinIds = [];
    }
    if (favouriteCoinIds.length === 0) {
        // Display error message, show return to home button and throw an error
        const errorMessage = "You haven't favourited any coins yet!";
        showTableErrorMessage(true, errorMessage);
        throw new Error();
    } else {
        marketData = getMarketData(displayCurrency, favouriteCoinIds);
    }

    return marketData; 
}

/**
 * Converts market data into Coin objects and stores in COIN_LIST
 * @param {*} marketData An array of coin data objects
 */
function populateCoinList(marketData) {
    COIN_LIST.length = 0;
    marketData.forEach(function(element) {
        let id = element["id"];
        let name = element["name"];
        let symbol = element["symbol"];
        let rank = element["market_cap_rank"];
        let currentPrice = element["current_price"];
        let totalVolume = element["total_volume"];
        let sparklineInSevenDays = element["sparkline_in_7d"];
        let coin = new Coin(id, name, symbol, rank, currentPrice, totalVolume, sparklineInSevenDays);
        COIN_LIST.push(coin);   // add coins into global scoped coin list
    });
}