"use strict";

// Holds information on a coin

class Coin {

    /**
     * @param {string} id id of coin
     * @param {string} symbol symbol of coin
     * @param {number} rank rank of coin
     * @param {string} name name of coin
     * @param {number} price price of coin
     * @param {number} volume volume of coin
     */
    constructor(id, name, symbol, rank, price, volume, sparklineInSevenDays) {
        this.id = id;
        this.name = name;
        this.symbol = symbol;
        this.rank = rank;
        this.price = price;
        this.volume = volume;
        this.sparklineInSevenDays = sparklineInSevenDays;
    }

    /**
     * Returns input symbol fully capitalized
     */
    getFormattedSymbol() {
        return this.symbol.toUpperCase();
    }

    /**
     * Returns price with complete cents or up to 4 significant figures for tokens under a dollar 
     * and currency prefix
     * @param {string} vsCurrency currency key
     */
    getFormattedPrice(vsCurrency) {
        const precision = 4;
        let formattedPrice;
        try {
            if (this.price % 1 == 0) {
                // Format exact dollar "1 -> 1.00"
                formattedPrice = this.price.toString() + ".00";
            } else if (this.price * 10 % 1 == 0) {
                // Format exact tenth dollar "1.1 -> 1.10"
                formattedPrice = this.price.toString() + "0";
            } else if (this.price < 1) {
                // Format significant figures up to 4 for tokens under a dollar
                formattedPrice = this.price.toPrecision(precision).toString();
            } else {
                formattedPrice = this.price.toString();
            }
        } finally {
            return CURRENCY[vsCurrency] + formattedPrice;
        }
    }

    /**
     * Returns volume with comma separation per thousands and currency prefix
     * @param {string} vsCurrency currency key
     */
    getFormattedVolume(vsCurrency) {
        let formattedVolume = Intl.NumberFormat('en-US').format(this.volume);
        return CURRENCY[vsCurrency] + formattedVolume;
    }
}