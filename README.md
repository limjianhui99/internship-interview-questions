# Internship Interview Questions
*Please include explanation along with your answers.*

1. Please describe yourself using JSON (include your internship start/end date, your current location).

> [Description](description.json)

2. Tell us about a newer (less than five years old) web technology you like and why?

> Flutter is a "new" technology introduced by Google in 2017. It is designed to be an open source kit. For my first Final Year Project, we decided to adopt it because of its features, namely, good documentation, extensive libraries and hot reload functionality. Frontend development on Flutter is fast because of the combination of widgets and hot reload which enable the display to be updated upon every save. From my previous experience developing an Android application with Java, it is much easier to produce a clean frontend with Flutter. However, I am particularly annoyed by the time (can take 2+ minutes) it takes to build the application for debugging every time I make changes to the backend code.

3. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?

> The easiest method would be through the use of a linked list. A node is used to store the element and the next node that it is linked to. This form of "Array" is dynamic because every time we add an element, we simply create a reference to it in the last node in the linked list.
>
> Its advantages is that it is fast for adding and removing elements (especially at the head and tail) while using a reasonable additional amount of memory to store node references.
>
> However its disadvantages are that it is that searching for certain elements may be slow or difficult because nodes must be traversed in one way.

4. Explain this block of code in Big-O notation.
    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```

> Let n = size. 
>
> The best case and worst case time complexity of this block of code is O(n^2) due to its inner and outer loops. There is no early termination.
>
> The space complexity of this block of code is O(n) assuming that the length of arr[] is equal to size

5. One of our developers built a page to display market information for over 500 cryptocurrencies in a single page. After several days, our support team started receiving complaints from our users that they are not able to view the website and it takes too long to load. What do you think causes this issue and suggest at least 3 ideas to improve the situation.

> 1. Inefficent algorithms were used to process cryptocurrency data. Review the code and update possible sources of inefficiencies such as multiple nested loops. Attempt to reduce time complexities using techniques like Dynamic Programming.

> 2. Insufficient server capacity or high server latency. If the server is hosted locally,  review the capacity and increase it if necessary. If most users are accessing the server far from the current server location, consider migrating to a remote server through 3rd party services.

> 3. Too many DOM elements are being processed causing long wait times and heavy load on the user's device. Try using pagination and only processing as well as displaying a reasonable number of DOM elements.

> 4. Developers should clear their cache. When debugging a web application, upon failure to retrieve certain elements, the browser can sometimes use its local cache to display information. This means that there might be existing errors but the developers are not able to detect it because the page still appears to load normally.

> 5. Too many GET requests. It is better to perform a single GET request for 500 (or another maximum) number of required elements rather than 500 individual GET requests. This also reduces the likelihood of users reaching their rate limits.

6. In Javascript, What is a "closure"? How does JS Closure works?

> A closure is an inner function that has access to its outer functions' variables. It preserves it's lexical environment (local variables, outer function variables and global variable). This means that if it is called later, it still has access to those variables 
> ```
> function getFunction() {
>     let integer = 1;
>     let closure = function() {return integer;} // there is no local variable integer, it will look for the outer function's integer variable
>     integer = 100;
>     return closure;
> }
>
> let returnFunction = getFunction();
> console.log(returnFunction()) // produces 100
> ```

7. In Javascript, what is the difference between var, let, and const. When should I use them?

> const defines a constant reference to a value. If applied to immutable data types like integers, it cannot be modified anymore. If applied to mutable data types like arrays, we can still modify the array since the reference is still the same.
> const should be used when we need want to ensure that certain reference is not overwritten (whether intentionally or accidentally) such as the speed of light etc.

> var is function scoped and let is block scoped. Most development is block scoped because it is less prone to errors. Let should be used in almost all cases instead of var.

8. Share with us one book that has changed your perspective in life. How did it change your life?

> Secrets of Self Made Millionaires by Adam Khoo. I first read this book when I was 12 years old due to its striking title. The book was extremely compelling and I couldn't stop reading it. It taught me a lot regarding topics about money, scale, compounded interest, passive income and delayed gratification. Inspired by his ideas, I think it is what led me to get involved in the cryptocurrency market in 2017 while I was studying in Sunway College. I was looking opportunities, took a risk and I'm glad it paid off.

9. What is the thing you believe in that is true, that most people disagree with?

> I believe that for a disciplined student, academic performance has an inverse relationship with the distance of your home to your academic institution. That is to say, the closer you stay to your academic institution, the more likely you are to perform better academically. People tend to say that's nonsense because everyone has to commute or travel a couple hours; it's normal and it shouldn't affect your performance. They claim that the extra hours saved from not travelling would have been used to do something else that is unproductive or lead to more procrastination due to the lack of urgency. I strongly disagree. I believe that doing something you like (that's considered unproductive) or not having the sense of urgency (to submit an assignment for example) alleviates stress and improves an individual's mental health. This in turn leads to improved productivity and better academic performance.

10. What are your thoughts on the subject of Arts and Humanities?

> I believe that the Arts and Humanities are essential subjects that helps us understand the world better. I have taken related electives and they have really taught me how far we've progressed as a civilization as well as appreciate our current high level of standard of living.

---
# Simple Coding Assessment

Build a Cryptocurrency Market dashboard showing market data.

requirement:
1. using the API endpoints found in https://www.coingecko.com/en/api, build a cryptocurrency market dashboard page.
2. The page should be able to list at least 20 coins.
3. The page should show price, volume, name, symbol of the coin.
4. The page should show the graph of 7 days data, using the **sparkline** returned from api. For example sparkline data can be obtained using [https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true](https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true) or [https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true](https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true)
5. The page should allow user to "favorite" coins.
6. build another page "favorite", to only show the favorite coins.
5. **(bonus)** The page should allow user to switch currency for ("usd", "myr", "btc"). The price and volume should display the number in the currency selected.
6. **(bonus)** Host this on a website, or a mobile app.
7. We will schedule a video call with you should we decide to proceed with your interview.
8. You must be able to demo your submission to us.

---
# Submission instruction

1. Fork this repo.
2. Creates a Merge Request in this repo after completion.
