"use strict";

const CURRENCY = {
    usd: "$",
    myr: "RM",
    btc: "\u20BF"
}
const SPARKLINE_ID_PREFIX = "sparkline_";
const FAVOURITE_ID_PREFIX = "favourite_";

// Element population functions

/**
 * Populates an input table with rows
 * @param {object} table reference to a table
 * @param {string} vsCurrency currency key
 */
function populateMainTable(table, vsCurrency) {
    // Populate table with rows
    COIN_LIST.forEach(function(coin) {
        let row = table.insertRow();
        row.className = "normal_row";
        populateRow(row, coin, vsCurrency);
    });
    
    // Populate cells with sparkline data if possible
    try {
        google.charts.load('current', {packages: ['corechart', 'line']}); // call only once
        google.charts.setOnLoadCallback(insertCellsWithSparkline);
    } catch(error) {
        console.log("Unable to GET sparklines");
    }

    // Populate cells with favourite buttons
    insertCellsWithFavouriteButton();
}

/**
 * Populates an input row with cells
 * @param {object} row reference to a row
 * @param {Coin} coin valid coin object
 * @param {string} vsCurrency currency key
 */
function populateRow(row, coin, vsCurrency) {
    const classNames = {
        leftAlign: "table_normal_item_left",
        rightAlign: "table_normal_item_right",
        centerAlign: "table_normal_item_center",
        sparkline: "table_sparkline"
    };
    
    let sparklineCellId = `${SPARKLINE_ID_PREFIX}${coin.id}`; // required to add graph
    let favouriteCellId = `${FAVOURITE_ID_PREFIX}${coin.id}`; // required to add favourite button
    // Insert cells and data into those cells
    insertDataIntoCell(row.insertCell(), "", classNames.centerAlign, favouriteCellId);
    insertDataIntoCell(row.insertCell(), coin.rank, classNames.centerAlign);
    insertDataIntoCell(row.insertCell(), coin.name, classNames.leftAlign);
    insertDataIntoCell(row.insertCell(), coin.getFormattedSymbol(), classNames.leftAlign);
    insertDataIntoCell(row.insertCell(), coin.getFormattedPrice(vsCurrency), classNames.rightAlign);
    insertDataIntoCell(row.insertCell(), coin.getFormattedVolume(vsCurrency), classNames.rightAlign);
    insertDataIntoCell(row.insertCell(), "Loading", classNames.sparkline, sparklineCellId); // Empty cell for sparkline
}

/**
 * Inserts data into a single cell
 * Accepts an optional callbackFunction paramter for further customization
 * @param {object} cell reference to a cell 
 * @param {string} text innerText for cell tag
 * @param {string} className className for cell tag
 * @param {string} id id for cell tag, optional
 * @param {Function} callbackFunction callback function for other customization 
 */
function insertDataIntoCell(cell, text, className, id=null, callbackFunction=null) {
    cell.innerText = text;
    cell.className = className;
    if (id !== null && typeof(id) === "string") {
        cell.id = id;
    }
    if (callbackFunction !== null && typeof(callbackFunction) === "function") {
        callbackFunction();
    }
}

/**
 * Line chart function
 * https://developers.google.com/chart/interactive/docs/gallery/linechart
 * Did not use available Sparkline provided by Google API due to future deprecation
 * Inserts cells with sparkline data
 * Configuration options: https://developers.google.com/chart/interactive/docs/gallery/linechart#configuration-options
 */
function insertCellsWithSparkline() {
    // Set options for line graph
    const options = {
        hAxis: {
            baselineColor: "transparent", // axis line
            textPosition: "none", // axis label
            gridlines: {
                color: "transparent"
            },
            logScale: false
        },
        vAxis: {
            baselineColor: "transparent", // axis line
            textPosition: "none", // axis label
            viewWindowMode: "maximized",
            gridlines: {
                color: "transparent"
            },
            logScale: false
        },
        legend: {
            position: "none"
        },
        chartArea: {
            // border
            backgroundColor: {
                stroke: "black",
                strokeWidth: "1",
            },
        },
        height: "100",
        width: "100%",
        colors: ["green"]
    };

    COIN_LIST.forEach(function(coin) {
        // Input columns
        let data = new google.visualization.DataTable();
        data.addColumn('number', 'Time');
        data.addColumn('number', coin.getFormattedSymbol());

        // Input rows
        let priceData = coin.sparklineInSevenDays.price;
        let dataRows = []
        for (let i = 0; i < priceData.length; i++) {
            dataRows.push([i, priceData[i]]);
        }
        data.addRows(dataRows);

        // Visualize data
        let sparklineCellId = `${SPARKLINE_ID_PREFIX}${coin.id}`;
        let chart = new google.visualization.LineChart(document.getElementById(sparklineCellId));
        chart.draw(data, options);
    });
}

/**
 * Inserts cells with favourite button
 */
function insertCellsWithFavouriteButton() {
    COIN_LIST.forEach(function(coin) {
        const favouriteCellId = `${FAVOURITE_ID_PREFIX}${coin.id}`;
        const favouriteButtonId = `${FAVOURITE_BUTTON_PREFIX}${coin.id}`;
        const favouriteButtonClassName = "favourite_button";
       
        let cell = document.getElementById(favouriteCellId);
        let button = document.createElement("button");
        button.className = favouriteButtonClassName;
        button.id = favouriteButtonId;
        button.value = false;
        // Use of closure here
        button.onclick = function() {
            toggleFavourite(button, coin.id);
        }
        cell.appendChild(button);
        
    })

    // Update favourite buttons for favourite coins
    updateButtonsWithFavourites();
}

// Set DOM elements

/**
 * Sets the table loader to either none or block
 * @param {boolean} displayBlock: true for "block", false for "none"
 */
function setTableLoaderDisplayBlock(displayBlock) {
    const loader = document.getElementById("table_loader");
    toggleElementDisplayBlock(loader, displayBlock);
}

/**
 * Sets the table error to either none or block
 * Optionally, you can set an errorMessage
 * @param {boolean} displayBlock: true for "block", false for "none"
 * @param {string} errorMessage: error message
 */
function showTableErrorMessage(displayBlock, errorMessage) {
    const tableError = document.getElementById("table_error");
    tableError.innerText = errorMessage;
    toggleElementDisplayBlock(tableError, displayBlock);
}

/**
 * Sets the display currency menu text
 */
function setDisplayCurrencyMenuText() {
    const displayCurrencyMenuText = document.getElementById("display_currency_menu_text");
    const displayCurrency = retrieveDisplayCurrency().toUpperCase();
    displayCurrencyMenuText.innerHTML = `<b>Display currency:</b> ${displayCurrency}`;
}

/**
 * Sets visibility of main table
 * @param {boolean} visible true for visible, false for hidden
 */
function setTableContainerVisibility(visible) {
    let tableContainer = document.getElementById("table_container");
    if (visible) {
        tableContainer.style.visibility = "visible";
    } else {
        tableContainer.style.visibility = "hidden";
    }
}

// Toggle DOM elements

/**
 * Sets the DOM element to either none or block
 * @param displayBlock: true for "block", false for "none"
 * @param {object} DOMElement: element from DOM 
 */
function toggleElementDisplayBlock(DOMElement, displayBlock) {
    if (displayBlock) {
        DOMElement.style.display = "block";
    } else {
        DOMElement.style.display = "none";
    }
}

/**
 * Switches display currency and repopulates the table
 * @param {string} displayCurrency 
 */
function switchDisplayCurrency(displayCurrency) {
    // Save display currency and update display currency menu text
    saveDisplayCurrency(displayCurrency);
    setDisplayCurrencyMenuText(displayCurrency);
    startRefreshTableFlow()
}

/**
 * Refresh table in with loaders
 */
function startRefreshTableFlow() {
    remakeTable();
    // Show loader and hide table
    setTableLoaderDisplayBlock(true);
    setTableContainerVisibility(false);
    // Remake the table
    initializePage();
}

/**
 * Remakes the table with only headers
 */
function remakeTable() {
    let mainTable = document.getElementById("main_table");
    let mainTableHeaderRow = document.getElementById("main_table_header_row");
    mainTable.innerHTML = "";
    mainTable.appendChild(mainTableHeaderRow);
}
